class UserFile < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :database

  belongs_to :user
  mount_uploader :database, DatabaseUploader
end
