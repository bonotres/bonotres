class HomeController < ApplicationController

  def index
    render :layout => "static"
  end

  def about_us
    render :layout => "static"
  end

  def carrers
    render :layout => "static"
  end

  def products
    render :layout => "static"
  end

  def index_backoffice
    @user_file = UserFile.new
    @user_file.user = current_user
  end

end
